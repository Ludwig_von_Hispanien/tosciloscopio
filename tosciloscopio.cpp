#include <unistd.h>
#include <stdio.h>

#include "TOsciloscopio.hh"

#define SERIAL_PORT "/dev/ttyACM0"


int main()
{

  TOsciloscopio tosciloscopio(4095);

  tosciloscopio.openPort(SERIAL_PORT, 115200);

  tosciloscopio.graficar();

  return 0;
  
}
