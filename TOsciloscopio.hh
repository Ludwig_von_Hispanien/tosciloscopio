#ifndef __TOSCILOSCOPIO__HH__
#define __TOSCILOSCOPIO__HH__

#include "Serial.hh"
#include "Grafica.hh"

class TOsciloscopio
{
public:
  TOsciloscopio(const size_t maxValue);
  virtual ~TOsciloscopio();

  char openPort(const char *Serial_P,const unsigned int Bauds);

  void graficar();
  
protected:

  Grafica *grafica;
  Serial *serial;

  char errorOpen;
  float corr;

  int getNumber();
  
};


TOsciloscopio::TOsciloscopio(const size_t maxValue)
{
  grafica =  new Grafica;
  serial = new Serial;

  corr = ((float)MAXLINES)/((float)maxValue);
  
}

TOsciloscopio::~TOsciloscopio()
{
  serial->closePort();

  delete grafica;
  delete serial;
}

char TOsciloscopio::openPort(const char *Serial_P,const unsigned int Bauds)
{
  errorOpen = serial->openPort(Serial_P, Bauds);
  return errorOpen;
}

void TOsciloscopio::graficar()
{
  
  if(errorOpen!=1)
    return;

  serial->DTR(true);
  serial->RTS(false);
  
  for(int i=0;i<10000;i++)
    {
      grafica->pushDatos( getNumber() );
      grafica->imprimir();
    }
  
}

int TOsciloscopio::getNumber()
{
  char n;
  char v[]="0000";
  
  for( int i = 0 ; i<20 ; i++ )
    {
      serial->readChar(&n);

      if( n == ' ' )
	{
	  for( int j = 0 ; j<4 ; j++)
	    {
	      serial->readChar(&n);
	      v[j] = n;
	    }
	  break;
	}
      
      
    }

  mvaddstr(0,0,v);
  
  return (int)(atoi(v)*corr);
}


#endif
