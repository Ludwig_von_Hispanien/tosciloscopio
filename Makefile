# tosciloscopio

include config.mk

SRC = tosciloscopio.cpp Serial.cpp
LIH = TOsciloscopio.hh Serial.hh Grafica.hh
OBJ = ${SRC:.cpp=.o}

all: tosciloscopio

.cpp.o:
	${CC} -c ${CFLAGS} $<

${OBJ}: TOsciloscopio.hh Serial.hh Grafica.hh

tosciloscopio: ${OBJ}
	${CC} -o $@ ${OBJ} ${CFLAGS}

clean:
	rm -f tosciloscopio ${OBJ} tosciloscopio-${VERSION}.tar.gz

dist: clean
	mkdir -p tosciloscopio-${VERSION}
	cp -R LICENSE Makefile README.md config.mk tosciloscopio.1 ${LIH} ${SRC} tosciloscopio-${VERSION}
	tar -cf tosciloscopio-${VERSION}.tar tosciloscopio-${VERSION}
	gzip tosciloscopio-${VERSION}.tar
	rm -rf tosciloscopio-${VERSION}

install: all
	cp -f tosciloscopio ${PREFIX}/bin
	chmod 755 ${PREFIX}/bin/tosciloscopio
	mkdir -p  ${MANPREFIX}/man1
	sed "s/VERSION/${VERSION}/g" < tosciloscopio.1 > ${DESTDIR}${MANPREFIX}/man1/tosciloscopio.1

uninstall:
	rm -r ${PREFIX}/bin/cuentas

.PHONY: all clean dist install uninstall
