# cuentas version
VERSION = 1E0.1a

# Paths
PREFIX = /usr/local
MANPREFIX = ${PREFIX}/share/man

# Include and libs
LIBS =  -lncurses -ltinfo

# Flafs
CFLAGS = -march=native -std=c++17 -Wall ${LIBS}

# Compiler
CC = g++
