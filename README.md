# TOsciloscopio

TOsciloscopio is a very simples osciloscopio without graphic resources.

## Requirements
In order to build TOsciloscopio you need only ncurses.

## Installation
Enter the following command to build and install.

`make`

`sudo make install`

## Running

runs with the command:

`tosciloscopio`

maybe you need sudo for opening the serial port.

`sudo tosciloscopio`
