#include "Serial.hh"

Serial::Serial()
{
}

Serial::~Serial()
{
    closePort();
}

/*
     Supported baud rate for Linux :
                        - 110
                        - 300
                        - 600
                        - 1200
                        - 2400
                        - 4800
                        - 9600
                        - 19200
                        - 38400
                        - 57600
                        - 115200

     return 1 success
     return -1 device not found
     return -2 error while opening the device
     return -3 error while getting port parameters
     return -4 Speed (Bauds) not recognized
     return -5 error while writing port parameters
     return -6 error while writing timeout parameters
*/

char Serial::openPort(const char *Serial_P,const unsigned int Bauds)
{

  fp = open(Serial_P, O_RDWR | O_NOCTTY | O_NDELAY);

  if(fp == -1)
    return -2;

  fcntl(fp, F_SETFL, FNDELAY);

  // Interface to asynchronous communications 
  struct termios options;

  tcgetattr(fp, &options);

  // Clear all the options
  bzero(&options, sizeof(options));

  speed_t   Speed;

  switch(Bauds)
    {
    case 110  :     Speed=B110; break;
    case 300  :     Speed=B300; break;
    case 600  :     Speed=B600; break;
    case 1200 :     Speed=B1200; break;
    case 2400 :     Speed=B2400; break;
    case 4800 :     Speed=B4800; break;
    case 9600 :     Speed=B9600; break;
    case 19200 :    Speed=B19200; break;
    case 38400 :    Speed=B38400; break;
    case 57600 :    Speed=B57600; break;
    case 115200 :   Speed=B115200; break;
    default :       return -4;
    }
  
  cfsetispeed(&options, Speed);
  cfsetospeed(&options, Speed);
  
  // Configure the serial port: 8 bits, no parity, no control
  options.c_cflag |= ( CLOCAL | CREAD |  CS8);
  options.c_iflag |= ( IGNPAR | IGNBRK );
  
  // Timer unused
  options.c_cc[VTIME]=0;
  
  // At least on character before satisfy reading
  options.c_cc[VMIN]=0;
  
  // Activate the settings
  tcsetattr(fp, TCSANOW, &options);
  
  return (1);

}


void Serial::closePort()
{
  close(fp);
}


char Serial::writeChar(const char Byte)
{
  if(write(fp,&Byte,1)!=1)
    return -1;
  return 1;
}

char Serial::writeBytes(const void *Buffer, const unsigned int NbBytes)
{
  if (write (fp,Buffer,NbBytes)!=(ssize_t)NbBytes)
    return -1;
  return 1;
}


//   return 1 success
//   return 0 Timeout reached
//   return -1 error while setting the Timeout
//   return -2 error while reading the byte

char Serial::readChar(char *pByte, unsigned int timeOut_ms)
{
  
  timeOut         timer;
  
  timer.initTimer();

  while (timer.elapsedTime_ms()<timeOut_ms || timeOut_ms==0)
    {
      switch (read(fp,pByte,1))
	{
	case 1  : return 1;
	case -1 : return -2;// Error while reading
	}
    }
  return 0;
}


//   return >0 success, return the number of bytes read
//   return -1 error while setting the Timeout
//   return -2 error while reading the byte
//   return -3 MaxNbBytes is reached

int Serial::readStringNoTimeOut(char *receivedString,char finalChar,unsigned int maxNbBytes)
{

  unsigned int    NbBytes=0;
  char            charRead;

  while(NbBytes<maxNbBytes)
    {

      charRead=readChar(&receivedString[NbBytes]);

      if(charRead==1)
        {
	  if(receivedString[NbBytes]==finalChar)
	    {
	      receivedString  [++NbBytes]=0;
	      return NbBytes;
            }
	  NbBytes++;
	}

        if(charRead<0)
	  return charRead;
    }
    // Buffer is full
    return -3;
}


//   return >=0 return the number of bytes read before timeout or requested data is completed
//   return -1 error while setting the Timeout
//   return -2 error while reading the byte

int Serial::readBytes (void *buffer,unsigned int maxNbBytes,unsigned int timeOut_ms, unsigned int sleepDuration_us)
{
    
  timeOut          timer;

  timer.initTimer();

  unsigned int     NbByteRead=0;

  while (timer.elapsedTime_ms()<timeOut_ms || timeOut_ms==0)
    {

      unsigned char* Ptr=(unsigned char*)buffer+NbByteRead;

      int Ret=read(fp,(void*)Ptr,maxNbBytes-NbByteRead);

      if (Ret==-1) return -2;

      if (Ret>0)
        {
	  NbByteRead+=Ret;
	  if (NbByteRead>=maxNbBytes)
	    return NbByteRead;
        }
        usleep (sleepDuration_us);
    }

  return NbByteRead;

}


char Serial::flushReceiver()
{
  tcflush(fp,TCIFLUSH);
  return true;
}


int Serial::available()
{    

  int nBytes=0;
  ioctl(fp, FIONREAD, &nBytes);
  return nBytes;

}


bool Serial::DTR(bool status)
{
  if (status)
    return this->setDTR();
  else
    return this->clearDTR();
}


bool Serial::setDTR()
{
  int status_DTR=0;
  ioctl(fp, TIOCMGET, &status_DTR);
  status_DTR |= TIOCM_DTR;
  ioctl(fp, TIOCMSET, &status_DTR);
  return true;
}

bool Serial::clearDTR()
{
  int status_DTR=0;
  ioctl(fp, TIOCMGET, &status_DTR);
  status_DTR &= ~TIOCM_DTR;
  ioctl(fp, TIOCMSET, &status_DTR);
  return true;
}


bool Serial::RTS(bool status)
{
  if (status)
    return this->setRTS();
  else
    return this->clearRTS();
}


bool Serial::setRTS()
{
  int status_RTS=0;
  ioctl(fp, TIOCMGET, &status_RTS);
  status_RTS |= TIOCM_RTS;
  ioctl(fp, TIOCMSET, &status_RTS);
  return true;
}


bool Serial::clearRTS()
{
  int status_RTS=0;
  ioctl(fp, TIOCMGET, &status_RTS);
  status_RTS &= ~TIOCM_RTS;
  ioctl(fp, TIOCMSET, &status_RTS);
  return true;
}


bool Serial::isCTS()
{
  int status=0;
  ioctl(fp, TIOCMGET, &status);
  return status & TIOCM_CTS;
}


bool Serial::isDSR()
{
  int status=0;
  ioctl(fp, TIOCMGET, &status);
  return status & TIOCM_DSR;
}


bool Serial::isDCD()
{
  int status=0;
  ioctl(fp, TIOCMGET, &status);
  return status & TIOCM_CAR;
}


bool Serial::isRI()
{
  int status=0;
  ioctl(fp, TIOCMGET, &status);
  return status & TIOCM_RNG;
}


bool Serial::isDTR()
{
  int status=0;
  ioctl(fp, TIOCMGET, &status);
  return status & TIOCM_DTR  ;
}


bool Serial::isRTS()
{
  int status=0;
  ioctl(fp, TIOCMGET, &status);
  return status & TIOCM_RTS;
}


timeOut::timeOut()
{
}


void timeOut::initTimer()
{
  gettimeofday(&previousTime, NULL);
}


unsigned long int timeOut::elapsedTime_ms()
{

  struct timeval CurrentTime;

  int sec,usec;

  gettimeofday(&CurrentTime, NULL);
  sec=CurrentTime.tv_sec-previousTime.tv_sec;
  usec=CurrentTime.tv_usec-previousTime.tv_usec;

  if (usec<0)
    {
      usec=1000000-previousTime.tv_usec+CurrentTime.tv_usec;
      sec--;
    }

  return sec*1000+usec/1000;
  
}
