#ifndef __SERIAL__HH__
#define __SERIAL__HH__

#include <sys/time.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/shm.h>
#include <termios.h>
#include <string.h>
#include <iostream>
#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>

//#define UNUSED(x) (void)(x)

class Serial
{
public:
  Serial();
  ~Serial();

  char openPort  (const char *Serial_P,const unsigned int Bauds);

  void closePort ();

  char writeChar (char);

  char readChar  (char *pByte,const unsigned int timeOut_ms=0);

  char writeBytes(const void *Buffer, const unsigned int NbBytes);

  int  readBytes (void *buffer,unsigned int maxNbBytes,const unsigned int timeOut_ms=0, unsigned int sleepDuration_us=100);

  char flushReceiver();

  int  available();

  bool DTR(bool status);
  bool setDTR();
  bool clearDTR();

  bool RTS(bool status);
  bool setRTS();
  bool clearRTS();

  bool isRI();

  bool isDCD();

  bool isCTS();

  bool isDSR();

  bool isRTS();

  bool isDTR();
  
private:

  int  readStringNoTimeOut  (char *String,char FinalChar,unsigned int MaxNbBytes);
  
  bool currentStateRTS;
  bool currentStateDTR;
  
  int  fp;

};


class timeOut
{
public:

  timeOut();

  void                initTimer();

  unsigned long int   elapsedTime_ms();

private:
    struct timeval      previousTime;
};


#endif
