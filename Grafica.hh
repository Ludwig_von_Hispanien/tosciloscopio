#ifndef __GRAFICA__HH__
#define __GRAFICA__HH__

#include <stdio.h>
#include <curses.h>
#include <signal.h>
#include <unistd.h>

#define MAXLINES  LINES-1
#define MAXCOLS   COLS-1


class Grafica
{
public:
  Grafica();
  virtual ~Grafica();

  void imprimir();

  void pushDatos(int x);
  
protected:

  int *datos;
  size_t first;

  void push(int x);
  
};


Grafica::Grafica() : first(0)
{

  initscr();
  signal(SIGINT, SIG_IGN);
  noecho();
  curs_set(0);
  nodelay(stdscr, TRUE);
  leaveok(stdscr, TRUE);
  scrollok(stdscr, FALSE);
  
  datos = new int[ MAXCOLS ];
  
  for( int i=0 ; i<MAXCOLS ; i++)
    *(datos + i) = 0;
  
}

Grafica::~Grafica()
{
  
  mvcur(0, MAXCOLS, MAXLINES, 0);
  endwin();

  delete datos;
  
}

void Grafica::imprimir()
{

  for ( int i = 0; i < (COLS - 1) ; i++ )
    mvaddch( MAXLINES - *(datos+i), i, '*' );
  
  getch();
  refresh();
  usleep(4000);

  refresh();
}

void Grafica::pushDatos( int x )
{

  if( x < 0 )
    x = 0;

  if( MAXLINES < x )
    x = MAXLINES;

  push(x);
  
}

void Grafica::push( int x )
{

  mvaddch( MAXLINES - *(datos+first), first , ' ' );
  
  *(datos + first) = x;
  first = (first + 1)%(COLS-1);
  
}


#endif
